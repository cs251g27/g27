# README #

This repository conatins the source files for a guide on learning some linux tools for scientific workflow. This work has been done as an assignment for the course CS251.

These tutorial demonstrates the following tools: 
1. Bash
2. Xfig
3. Octave
4. Gnuplot
5. LaTeX
6. Html
7. Git & Bitbucket

# Contributors #
- Swathi Krishna
- Ishika Soni
- Jeenu Sri Jaswanth Chandra
- Sanjay Bharat
